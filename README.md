# WebHIS for PHR

## Git Clone
    git clone https://gitlab.com/eisenhiem/proposal-it.git api-proposal-it
    cd api-proposal-it

## Install

    npm i -g pm2 
    npm i -g ts-node
    npm i -g typescript
    npm i -g nodemon
    
    npm i --force 

## COPY File
    cp _config.conf config.conf

## Edit config.conf
    DB_HOST=localhost
    DB_PORT=5410
    DB_NAME=hidb
    DB_USER=hidb
    DB_PASSWORD=xxxxxxxx

    SECRET_KEY=12222222222

    MAX_CONNECTION_PER_MINUTE=100000

    PORT=30012
    HOST=0.0.0.0

## Start
    npm start

## Pm2 
    pm2 start nodemon --name api-proposal-it


## Update

