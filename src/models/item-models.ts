import { Knex } from 'knex';

export class ItemModel {

    async list(db: Knex) {

        const datas = await db('item');

        const result: any = {
            ok: true,
            text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
            rows: datas
        }
        return result;

    }

    async select_id(db: Knex, id: any) {

        let datas = await db('item')
            .where('item.id', id);

        const result: any = {
            ok: true,
            text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
            rows: datas[0]
        }
        return result;

    }

    async save(db: Knex, data: any) {

        for (const property in data) { if (data[property] === "") { delete data[property]; } }
        // delete data.hn;
        let saved_rs = await db('item')
            .insert(data);

        let datas: any = await db('item').where('id', saved_rs[0]);

        const result: any = {
            ok: true,
            text: "บันทึกข้อมูลเรียบเสร็จร้อยแล้ว",
            rows: datas
        };
        return result;

    }

    async update(db: Knex, id: any, data: any) {

        for (const property in data) { if (data[property] === "") { delete data[property]; } }
        const rs = await db('item').where('id', id);

        if (data.d_update == rs[0].d_update.toISOString()) {

            delete data.d_update;

            const rows = await db('item')
                .where("id", id)
                .update(data);
            const datas = await db('item').where('id', id);
            const result: any = {
                ok: true,
                text: "แก้ไขข้อมูลเรียบเสร็จร้อยแล้ว",
                "rows": datas[0]
            };
            return result;
        } else {
            const result: any = {
                ok: false,
                text: "มีการแก้ไขข้อมูลโดยผู้ใช้งานอื่นก่อนที่จะมีการจัดเก็บ",
                "rows": rs[0]
            };
            return result;
        }

    }

    async delete(db: Knex, id: any) {

        const rs = await db('item').where('id', id);

        if (rs.length > 0) {

            const rows = await db('item')
                .where("id", id)
                .del();

            const result: any = {
                ok: true,
                text: "ลบข้อมูลเรียบเสร็จร้อยแล้ว",
                "rows": rs[0]
            };
            return result;

        } else {
            const result: any = {
                ok: false,
                text: "ไม่พบข้อมูลที่ต้องการลบ",
                "rows": rs[0]
            };
            return result;
        }

    }

}