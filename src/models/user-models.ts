import { Knex } from 'knex'
const hash = require('object-hash');
import * as crypto from 'crypto'

export class UserModel {
    tableName = 'user';
    selecteView: string = `select * from user `;
  
    async list(db: Knex, req: any) {
      let info: any = req.body;
      let xorderby = "id";
  
      for (const property in info) { if (info[property] === "") { delete info[property]; } }
  
      let limit :string = "";

      if(info.limit) { limit = ' limit ' + info.limit; }
      if(info.offset) { limit += ' offset ' + info.offset; }

      if (info.orderby) { xorderby = info.orderby; }
  
      let criteria = " Where ";
   
      if (info.id) { criteria += ` id = ${info.id} AND `; }
      if (info.username) { criteria += ` username = '${info.username}' AND `; }
      if (info.role_id) { criteria += ` role_id = ${info.role_id} AND `; }
      if (info.is_active) { criteria += ` is_active = '${info.is_active}' AND `; }
  
      criteria += " 1=1";
    
      let sql = ` ${this.selecteView} ${criteria} order by ${xorderby} desc ${limit}`;
      let datas = await db.raw(sql);
      const result: any = {
        ok: true,
        // title: 'ข้อมูลทะเบียนสิทธิ์ผู้รับบริการ',
        text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
        rows: datas[0]
      }
      return result;
    }

    async listAll(db: Knex) {
      const datas = await db(this.tableName);
      const result: any = {
        ok: true,
        // title: 'ข้อมูลทะเบียนสิทธิ์ผู้รับบริการ',
        text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
        rows: datas
      }
      return result;
    }
  
    async listAllActive(db: Knex) {
      const datas = await db(this.tableName).where('is_active', '1');
      const result: any = {
        ok: true,
        // title: 'ข้อมูลทะเบียนสิทธิ์ผู้รับบริการ',
        text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
        rows: datas
      }
      return result;
    }

    async select_id(db: Knex, id: any) {
      let datas = await db(this.tableName).where('id', id);
      const result: any = {
        ok: true,
        text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
        rows: datas[0]
      }
      return result;
    }
  
    async save(db: Knex, data: any) {
      for (const property in data) { if (data[property] === "") { delete data[property]; } }
      // delete data.hn;
      let saved_rs = await db(this.tableName)
        .insert(data);        
    //   let dat = saved_rs;
      let datas: any = await db(this.tableName).where('id', saved_rs);
      const result: any = {
        ok: true,
        text: "บันทึกข้อมูลเรียบเสร็จร้อยแล้ว",
        rows: datas[0]
      };
      return result;
    }
  
  
    async update(db: Knex, id: any, data: any) {
      for (const property in data) { if (data[property] === "") { delete data[property]; } }
      const rs = await db(this.tableName).where('id', id);
  
      if (data.d_update == rs[0].d_update.toISOString()) {

        delete data.d_update;        

        const rows = await db(this.tableName)
          .where("id", id)
          .update(data);
        const datas = await db(this.tableName).where('id', id);
        const result: any = {
          ok: true,
          text: "แก้ไขข้อมูลเรียบเสร็จร้อยแล้ว",
          "rows": datas[0]
        };
        return result;
      } else {
        const result: any = {
          ok: false,
          text: "มีการแก้ไขข้อมูลโดยผู้ใช้งานอื่นก่อนที่จะมีการจัดเก็บ",
          "rows": rs[0]
        };
        return result;
      }
    }
  
    delete(db: Knex, id: any) {
      return db(this.tableName)
        .where('id', id)
        .del();
    }
  
    async login(db: Knex, req: any) {
      let info: any = req.body;
      for (const property in info) { if (info[property] === "") { delete info[property]; } }
  
      let password_hash: any;
      if (info.password) { password_hash = crypto.createHash('md5').update(info.password).digest('hex') }
      // let sql = `${this.selecteView} 

      // console.log(sql);
      let datas = await db(this.tableName)
        .leftJoin('profile', 'profile.user_id', 'user.id')
        .leftJoin('role', 'role.role_id', 'user.role_id')
        .where('user.username', info.username)
        .andWhere('user.password_hash', password_hash)
        .andWhere('user.is_active', '1')
        .select('user.id', 'user.username', 'user.role_id', 'role.role_name', 'profile.title','profile.fullname','profile.position', 'user.d_update');
  
      console.log(datas);

      return datas;
    }
}
