import { FastifyInstance, FastifyRequest, FastifyReply } from "fastify"

export default async (fastify: FastifyInstance) => {

  fastify.get('/', async (request: FastifyRequest, reply: FastifyReply) => {
    reply.code(200).send({ message: 'PETITION CENTER  -->Fastify, RESTful API services! 20230303' });
  });

  fastify.get('/sign-token', async (request: FastifyRequest, reply: FastifyReply) => {
    const token = fastify.jwt.sign({ foo: 'PETITION CENTER' }, { expiresIn: '1d' });
    reply.send({ token: token });
  })

}
