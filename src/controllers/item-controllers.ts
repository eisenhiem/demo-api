import { FastifyInstance, FastifyRequest, FastifyReply } from "fastify"
import { ItemModel } from '../models/item-models';

const fromModel = new ItemModel();

export default async (fastify: FastifyInstance) => {

    const db = fastify.db;

    fastify.get('/', async (request: FastifyRequest, reply: FastifyReply) => {
        reply.code(200).send({ message: 'Item CENTER  -->Fastify, RESTful API services! 20230303' });
    });

    fastify.get('/list', async (request: FastifyRequest, reply: FastifyReply) => {
        try {
            var rs = await fromModel.list(db);
            // console.log(rs);
            reply.code(200).send(rs);
        } catch (error: any) {
            reply.code(500).send({
                ok: false,
                text: "การอ่านข้อมูลเกิดความผิดพลาด",
                error: error.message
            });
        }

    });

    fastify.post('/save', async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request;
        const data = req.body;

        try {
            var rs = await fromModel.save(db, data);
            // console.log(rs);
            reply.code(200).send(rs);
        } catch (error: any) {
            req.log.error(error);
            reply.code(500).send({
                ok: false,
                text: "การอ่านข้อมูลเกิดความผิดพลาด",
                error: error.message
            });

        }
    });

    fastify.put('/update', async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request;
        const data = req.body;
        const id = req.header.id;

        try {
            var rs = await fromModel.update(db, id, data);
            // console.log(rs);
            reply.code(200).send(rs);
        } catch (error: any) {
            req.log.error(error);
            reply.code(500).send({
                ok: false,
                text: "การอ่านข้อมูลเกิดความผิดพลาด",
                error: error.message
            });

        }
    });

    fastify.delete('/delete', async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request;
        const id = req.header.id;

        try {
            var rs = await fromModel.delete(db, id);
            // console.log(rs);
            reply.code(200).send(rs);
        } catch (error: any) {
            req.log.error(error);
            reply.code(500).send({
                ok: false,
                text: "การอ่านข้อมูลเกิดความผิดพลาด",
                error: error.message
            });

        }
    });

    fastify.post('/select_id', async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request;
        const id = req.headers;

        console.log(id);
        try {
            //var rs = await fromModel.select_id(db, id);
            // console.log(rs);
            reply.code(200).send(id);
        } catch (error: any) {
            req.log.error(error);
            reply.code(500).send({
                ok: false,
                text: "การอ่านข้อมูลเกิดความผิดพลาด",
                error: error.message
            });

        }
    });
}