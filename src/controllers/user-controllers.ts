import { FastifyInstance, FastifyRequest, FastifyReply } from "fastify"
import { UserModel } from '../models/user-models'

const fromImportModel = new UserModel();

export default async (fastify: FastifyInstance) => {
const db = fastify.db;

  fastify.get('/', async (request: FastifyRequest, reply: FastifyReply) => {
    reply.code(200).send({ message: 'User Controllers  -->Fastify, RESTful API services!!!!!! 20221102' });
  });

  fastify.get('/:id', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const id = req.params.id;
    try {
      var rs = await fromImportModel.select_id(db, id);
      // console.log(rs);
      reply.code(200).send(rs);
    } catch (error: any) {
      req.log.error(error);
      reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
      });
    }
  });

  fastify.post('/list-all', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    try {
      var rs = await fromImportModel.listAll(db);
      // console.log(rs);
      reply.code(200).send(rs);
    } catch (error: any) {
      req.log.error(error);
      reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
      });
    }
  });

  fastify.post('/list-all-active', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    try {
      var rs = await fromImportModel.listAllActive(db);
      // console.log(rs);
      reply.code(200).send(rs);
    } catch (error: any) {
      req.log.error(error);
      reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
      });
    }
  });

  fastify.post('/login', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    try {
      var rs = await fromImportModel.login(db, req);
      // console.log(rs);
      reply.code(200).send(rs);
    } catch (error: any) {
      req.log.error(error);
      reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
      });
    }
  });

  fastify.post('/list', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    try {
      var rs = await fromImportModel.list(db, req);
      // console.log(rs);
      reply.code(200).send(rs);
    } catch (error: any) {
      req.log.error(error);
      reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
      });
    }
  });

  fastify.post('/', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const info = req.body;

    try {
      var rs = await fromImportModel.save(db, info);
      reply.code(200).send(rs);
    } catch (error: any) {
      req.log.error(error);
      reply.code(500).send({
        ok: false,
        text: "การเพิ่มข้อมูลเกิดความผิดพลาด",
        error: error.message
      });
    }
  });

  //   UPDATE
  fastify.put('/:id', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const info = req.body;
    const id = req.params.id;
    try {
      var rs = await fromImportModel.update(db, id, info);
      reply.code(200).send(rs);
    } catch (error: any) {
      req.log.error(error);
      var rs = await fromImportModel.select_id(db, id);
      reply.code(500).send({
        ok: false,
        text: "การแก้ไขข้อมูลเกิดความผิดพลาด",
        error: error.message,
        rows: rs
      });
    }
  });


  fastify.delete('/:id', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const id = req.params.id;

    try {
      var rs = await fromImportModel.delete(db, id);
      reply.code(200).send({ ok: true, "text": `ลบข้อมูลเรียบร้อยแล้ว ${rs} รายการ`, rows: rs });
    } catch (error: any) {
      req.log.error(error);
      reply.code(500).send({
        ok: false,
        text: "การลบข้อมูลเกิดความผิดพลาด",
        error: error.message
      });
    }
  });



}