import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify';
import { UserModel } from '../models/user-models'

import moment from 'moment-timezone';

const fromImportModel = new UserModel();

export default async (fastify: FastifyInstance) => {
  const db = fastify.db;

  fastify.get('/', async (request: FastifyRequest, reply: FastifyReply) => {
    reply.code(200).send({ message: 'login  -->Fastify, RESTful API services! HI720220220' });
  });

  fastify.post('/', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;

    try {
      var rs = await fromImportModel.login(db, req);
      // console.log(rs);

      if (rs.length > 0) {

        const user: any = rs[0];

        const token = fastify.jwt.sign({ iss: "HIS-Import-App" }, { expiresIn: '1d' },);

        const payload: any = user;

        payload.accessToken = await token;
        payload.tokenType = "Bearer";
        if (user.fullname) {
          payload.name = `${user.fullname}`;
        } else {
          payload.name = ``;
        }

        payload.status = "online";

        let info: any = {
          d_update: payload.d_update,
          last_login_at: moment(new Date()).tz('Asia/Bangkok').format('YYYY-MM-DD HH:mm:ss')
        }

        let re_update: any = await fromImportModel.update(db, payload.id, info);
        reply.code(200).send({
          ok: true,
          text: "เข้าระบบเสร็จเรียบร้อยแล้ว",
          rows: payload
        });

      } else {
        reply.status(200).send({ ok: false,  text: "ชื่อผู้ใช้งาน หรือรหัสผ่าน ไม่ถูกต้อง",   message: 'Login failed' })
      }


    } catch (error: any) {
      req.log.error(error);
      reply.code(500).send({
        ok: false,
        text: "เข้าระบบเกิดความผิดพลาด",
        error: error.message
      });
    }
  });

}


