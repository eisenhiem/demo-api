import { FastifyInstance } from 'fastify';

export default async (fastify: FastifyInstance) => {
  fastify.register(require('./controllers/index-controllers'), { prefix: '/', logger: true });
  fastify.register(require('./controllers/user-controllers'), { prefix: '/user', logger: true });
  fastify.register(require('./controllers/item-controllers'), { prefix: '/item', logger: true });
  fastify.register(require('./controllers/login-controllers'), { prefix: '/login', logger: true });
}
