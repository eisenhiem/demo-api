import * as knex from 'knex'
import * as jsonwebtoken from 'jsonwebtoken';
import { AxiosInstance } from 'axios';
declare module 'fastify' {
  interface FastifyInstance {
    db: knex
    pg: knex
    jwt: jsonwebtoken
    authenticate: any
    axios: AxiosInstance | any
    qrcode: any
  }

  interface FastifyRequest {
    jwtVerify: any
    authenticate: any
    user: any
    file: any
    files: any[]
  }

}

